package com.devcamp.s10.chapterlessonapi.model;
import java.util.Set;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Range;
@Entity
@Table(name = "chapters")
public class Chapter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Phải phập tên chương")
    @Column(name = "chapter_name", unique = true)
    private String chapterName;

    @NotBlank(message = "Phải nhập thông tin chương")
    @Column(name = "chapter_info")
    private String chapterInfo;

    @NotEmpty (message = "Phải nhập tên người dịch")
    @Column(name = "translator")
    private String translator;


    @NotEmpty(message = "Phải nhập giá trị trang")
    @Range(min=1, message = "Nhập giá trị từ 1 trở lên")
    @Column(name = "page")
    private String page;

    @OneToMany(mappedBy = "chapter", cascade = CascadeType.ALL)
    private Set<Lesson> lessons;

   

    public Chapter(@NotEmpty(message = "Phải phập tên chương") String chapterName,
            @NotBlank(message = "Phải nhập thông tin chương") String chapterInfo,
            @NotEmpty(message = "Phải nhập tên người dịch") String translator,
            @NotEmpty(message = "Phải nhập giá trị trang") @Range(min = 1, message = "Nhập giá trị từ 1 trở lên") String page,
            Set<Lesson> lessons) {
        this.chapterName = chapterName;
        this.chapterInfo = chapterInfo;
        this.translator = translator;
        this.page = page;
        this.lessons = lessons;
    }

    public Chapter() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName;
    }

    public String getChapterInfo() {
        return chapterInfo;
    }

    public void setChapterInfo(String chapterInfo) {
        this.chapterInfo = chapterInfo;
    }

    public String getTranslator() {
        return translator;
    }

    public void setTranslator(String translator) {
        this.translator = translator;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Set<Lesson> getLessons() {
        return lessons;
    }

    public void setLessons(Set<Lesson> lessons) {
        this.lessons = lessons;
    }

    
}
