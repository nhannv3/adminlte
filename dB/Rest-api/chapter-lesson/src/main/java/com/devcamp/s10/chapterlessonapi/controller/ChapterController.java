package com.devcamp.s10.chapterlessonapi.controller;

import java.util.*;


import javax.validation.Valid;

import com.devcamp.s10.chapterlessonapi.model.Chapter;
import com.devcamp.s10.chapterlessonapi.reponsitory.ChapterReponsitory;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class ChapterController {
    @Autowired
    ChapterReponsitory pChapterReponsitory;

    @GetMapping("/chapter/all")
    public ResponseEntity<List<Chapter>> getALlChapters() {
        return new ResponseEntity<>(pChapterReponsitory.findAll(), HttpStatus.OK);
    }

    @GetMapping("/chapter/{id}")
    public ResponseEntity<Object> getChapterById(@PathVariable long id) {
        if (pChapterReponsitory.findById(id).isPresent())
            return new ResponseEntity<>(pChapterReponsitory.findById(id).get(), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/chapter/create")
    public ResponseEntity<Object> createChapter(@Valid @RequestBody Chapter newChapter) {
        try {
            pChapterReponsitory.save(newChapter);
            return new ResponseEntity<>(newChapter, HttpStatus.CREATED);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Chapter: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/chapter/update/{id}")
    public ResponseEntity<Object> updateChapter(@PathVariable long id, @Valid @RequestBody Chapter pChapter) {
        try {
            Optional<Chapter> chapterData = pChapterReponsitory.findById(id);
            if (chapterData.isPresent()) {
                Chapter _chapter = chapterData.get();
                _chapter.setChapterName(pChapter.getChapterName());
                _chapter.setChapterInfo(pChapter.getChapterInfo());
                _chapter.setPage(pChapter.getPage());
                _chapter.setTranslator(pChapter.getTranslator());
                _chapter.setLessons(pChapter.getLessons());
                pChapterReponsitory.save(_chapter);
                return new ResponseEntity<>(_chapter, HttpStatus.OK);
            } else
                return ResponseEntity.badRequest().body("Failed to get specified Chapter: " + id + "  for update.");
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Chapter: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/chapter/delete/{id}")
    public ResponseEntity<Object> deleteChapter(@PathVariable long id) {
        try {
            if (pChapterReponsitory.findById(id).isPresent()) {
                pChapterReponsitory.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else
                return ResponseEntity.badRequest().body("Failed to get specified Chapter: " + id + "  for delete.");
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            // Hiện thông báo lỗi tra back-end
            // return new ResponseEntity<>(e.getCause().getCause().getMessage(),
            // HttpStatus.INTERNAL_SERVER_ERROR);
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete specified Chapter: " + e.getCause().getCause().getMessage());
        }
    }
    @DeleteMapping("/chapter/delete/all")
    public ResponseEntity<Object> deleteAllChapter() {
        try {
                pChapterReponsitory.deleteAll();;
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete specified Chapter: " + e.getCause().getCause().getMessage());
        }
    }

}
