package com.devcamp.s10.chapterlessonapi.model;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

import org.hibernate.validator.constraints.Range;
@Entity
@Table(name = "lessons")
public class Lesson {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotEmpty(message = "Phải nhập tên bài")
    @Column(name = "lesson_name",unique = true)
    private String lessonName;

    @NotBlank(message = "Phải nhập giới thiệu bài")
    @Column(name = "lesson_info")
    private String lessonInfo;

    @NotNull(message = "phải nhập trang")
    @Range(min=1, message = "Nhập giá trị từ 1 trở lên")
    @Column(name = "page")
    private String page;

    @ManyToOne
    @JsonIgnore
    @JoinColumn(name="chapter_id")
    private Chapter chapter;

    public Lesson(@NotEmpty(message = "Phải nhập tên bài") String lessonName,
            @NotBlank(message = "Phải nhập giới thiệu bài") String lessonInfo,
            @NotNull(message = "phải nhập trang") @Range(min = 1, message = "Nhập giá trị từ 1 trở lên") String page,
            Chapter chapter) {
        this.lessonName = lessonName;
        this.lessonInfo = lessonInfo;
        this.page = page;
        this.chapter = chapter;
    }

    public Lesson() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLessonName() {
        return lessonName;
    }

    public void setLessonName(String lessonName) {
        this.lessonName = lessonName;
    }

    public String getLessonInfo() {
        return lessonInfo;
    }

    public void setLessonInfo(String lessonInfo) {
        this.lessonInfo = lessonInfo;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public Chapter getChapter() {
        return chapter;
    }

    public void setChapter(Chapter chapter) {
        this.chapter = chapter;
    }
    
    
}
