package com.devcamp.s10.chapterlessonapi.controller;

import java.util.*;


import javax.validation.Valid;

import com.devcamp.s10.chapterlessonapi.model.Chapter;
import com.devcamp.s10.chapterlessonapi.model.Lesson;
import com.devcamp.s10.chapterlessonapi.reponsitory.ChapterReponsitory;
import com.devcamp.s10.chapterlessonapi.reponsitory.LessonReponsitory;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class LessonController {
    @Autowired
    LessonReponsitory pLessonReponsitory;

    @Autowired
    ChapterReponsitory pChapterReponsitory;

    @GetMapping("/lesson/all")
    public ResponseEntity<List<Lesson>> getAllLessons() {
        return new ResponseEntity<>(pLessonReponsitory.findAll(), HttpStatus.OK);
    }

    @GetMapping("/chapter/{chapterId}/lesson")
    public ResponseEntity<List<Lesson>> getLessonByChapterId(@PathVariable("chapterId") long chapterId) {
        if (pLessonReponsitory.findByChapterId(chapterId).isEmpty())
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        else
            return new ResponseEntity<>(pLessonReponsitory.findByChapterId(chapterId), HttpStatus.OK);
    }

    @GetMapping("/lesson/{id}")
    public ResponseEntity<Object> getLessonById(@PathVariable long id) {
        if (pLessonReponsitory.findById(id).isPresent())
            return new ResponseEntity<>(pLessonReponsitory.findById(id), HttpStatus.OK);
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @PostMapping("/lesson/create/{id}")
    public ResponseEntity<Object> createLesson(@PathVariable long id, @Valid @RequestBody Lesson newLesson) {
        try {
            Optional<Chapter> chapterData = pChapterReponsitory.findById(id);
            if (chapterData.isPresent()) {
                newLesson.setChapter(chapterData.get());
                pLessonReponsitory.save(newLesson);
                return new ResponseEntity<>(newLesson, HttpStatus.CREATED);
            } else
                return ResponseEntity.badRequest().body("Failed to get specified Chapter: " + id + "  for create.");
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Create specified Lesson: " + e.getCause().getCause().getMessage());
        }
    }

    @PutMapping("/lesson/update/{id}")
    public ResponseEntity<Object> updaeLesson(@PathVariable long id, @Valid @RequestBody Lesson pLesson) {
        try {
            Optional<Lesson> lessonData = pLessonReponsitory.findById(id);
            if (lessonData.isPresent()) {
                Lesson _lesson = lessonData.get();
                _lesson.setLessonName(pLesson.getLessonName());
                _lesson.setLessonInfo(pLesson.getLessonInfo());
                _lesson.setPage(pLesson.getPage());
                _lesson.setChapter(pLesson.getChapter());
                pLessonReponsitory.save(_lesson);
                return new ResponseEntity<>(_lesson, HttpStatus.OK);
            } else
                return ResponseEntity.badRequest().body("Failed to get specified Lesson: " + id + "  for update.");

        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Update specified Lesson: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/lesson/delete/{id}")
    public ResponseEntity<Object> deleteLesson(@PathVariable long id) {
        try {
            if (pLessonReponsitory.findById(id).isPresent()) {
                pLessonReponsitory.deleteById(id);
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            } else
                return ResponseEntity.badRequest().body("Failed to get specified Lesson: " + id + "  for delete.");
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete specified Lesson: " + e.getCause().getCause().getMessage());
        }
    }

    @DeleteMapping("/lesson/delete/all")
    public ResponseEntity<Object> deleteAllLesson() {
        try {
            pLessonReponsitory.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            System.out.println("+++++++++++++++++++++::::: " + e.getCause().getCause().getMessage());
            return ResponseEntity.unprocessableEntity()
                    .body("Failed to Delete specified Lesson: " + e.getCause().getCause().getMessage());
        }
    }

}
