package com.devcamp.s10.chapterlessonapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChapterLessonApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChapterLessonApiApplication.class, args);
	}

}
