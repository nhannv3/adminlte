package com.devcamp.s10.chapterlessonapi.reponsitory;

import com.devcamp.s10.chapterlessonapi.model.Chapter;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChapterReponsitory extends JpaRepository<Chapter, Long>{
    
}
