package com.devcamp.s10.chapterlessonapi.reponsitory;

import java.util.List;

import com.devcamp.s10.chapterlessonapi.model.Lesson;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LessonReponsitory extends JpaRepository<Lesson, Long>{
    List<Lesson> findByChapterId(long chapterId);
}
