package com.devcamp.pizza365.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
@Entity
@Table(name = "employees")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@NotEmpty(message = "Input LastName")
    @Column(name = "last_name", nullable = false)
    private String lastName;

	@NotEmpty(message = "Input FirstName")
    @Column(name = "first_name", nullable = false)
    private String firstName;

	@NotEmpty(message = "Input Extension")
    @Column(name = "extension", nullable = false)
   private String extension;

	@NotEmpty(message = "Input Email")
	@Column(name = "email", nullable = false)
   private String email;

	@NotEmpty(message = "Input OfficeCode")
	@Column(name = "office_code", nullable = false)
   private long officeCode;

   @Column(name = "report_to")
   private long reportTo;

	@NotEmpty(message = "Input JobTitle")
	@Column(name = "job_title", nullable = false)
   private String jobTitle;

public Employee(String lastName, String firstName, String extension, String email, long officeCode, long reportTo,
        String jobTitle) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.extension = extension;
    this.email = email;
    this.officeCode = officeCode;
    this.reportTo = reportTo;
    this.jobTitle = jobTitle;
}

public Employee() {
}

public long getId() {
    return id;
}

public void setId(long id) {
    this.id = id;
}

public String getLastName() {
    return lastName;
}

public void setLastName(String lastName) {
    this.lastName = lastName;
}

public String getFirstName() {
    return firstName;
}

public void setFirstName(String firstName) {
    this.firstName = firstName;
}

public String getExtension() {
    return extension;
}

public void setExtension(String extension) {
    this.extension = extension;
}

public String getEmail() {
    return email;
}

public void setEmail(String email) {
    this.email = email;
}

public long getOfficeCode() {
    return officeCode;
}

public void setOfficeCode(long officeCode) {
    this.officeCode = officeCode;
}

public long getReportTo() {
    return reportTo;
}

public void setReportTo(long reportTo) {
    this.reportTo = reportTo;
}

public String getJobTitle() {
    return jobTitle;
}

public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
}

   
}
