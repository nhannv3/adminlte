package com.devcamp.pizza365.model;

import java.math.BigDecimal;

public interface IPayCustomer {
    public String getfullname();
    public Long getid();
    public String getcheck_number();
    public String getpayment_date();
    public BigDecimal getammount();
}
