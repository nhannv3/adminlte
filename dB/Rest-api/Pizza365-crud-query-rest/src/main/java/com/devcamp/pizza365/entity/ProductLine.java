package com.devcamp.pizza365.entity;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "product_lines")
public class ProductLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@NotEmpty(message = "Input Description")
    @Column(name = "description", nullable = false)
    private String description;

	@NotEmpty(message = "Input Product Line")
    @Column(name = "product_line", nullable = false)
    private String productLine;

    @OneToMany(mappedBy = "productLine", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Product> products;

    public ProductLine() {
    }

    public String getProductLine() {
        return productLine;
    }

    public void setProductLine(String productLine) {
        this.productLine = productLine;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
 
}
