package com.devcamp.pizza365.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "order_date", nullable = false)
   private Date ordeDate;

   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "required_date", nullable = false)
   @JsonFormat(pattern = "yyyy-MM-dd")
   private Date requiredDate;

   
   @Temporal(TemporalType.TIMESTAMP)
   @Column(name = "shipped_date")
   @JsonFormat(pattern = "yyyy-MM-dd")
   private Date shippedDate;
   
    @Column(name = "status")
    private String status;

    @Column(name = "comments")
    private String comments;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    @JsonIgnore
    private Customer customer1;

    @OneToMany(mappedBy = "order")
    @JsonIgnore
    private List<OrderDetail> orderDetails;

    public Order() {
    }

    public Order(Date ordeDate, Date requiredDate, Date shippedDate, String status, String comments, Customer customer1,
            List<OrderDetail> orderDetails) {
        this.ordeDate = ordeDate;
        this.requiredDate = requiredDate;
        this.shippedDate = shippedDate;
        this.status = status;
        this.comments = comments;
        this.customer1 = customer1;
        this.orderDetails = orderDetails;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getOrdeDate() {
        return ordeDate;
    }

    public void setOrdeDate(Date ordeDate) {
        this.ordeDate = ordeDate;
    }

    public Date getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(Date requiredDate) {
        this.requiredDate = requiredDate;
    }

    public Date getShippedDate() {
        return shippedDate;
    }

    public void setShippedDate(Date shippedDate) {
        this.shippedDate = shippedDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    @JsonIgnore
    public Customer getCustomer() {
        return customer1;
    }

    public void setCustomer(Customer customer) {
        this.customer1 = customer;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }

    
}
 