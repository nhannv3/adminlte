package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerService {
    @Autowired
    ICustomerRepository pCustomerRepository;

    public List<Customer> findAllCustomers() {
        return pCustomerRepository.findAll();
    }

    public Object findCustomerById(Long id) {
        return pCustomerRepository.findById(id).isPresent()
                ? pCustomerRepository.findById(id).get()
                : null;
    }

    public Object createCustomer(Customer newCustomer) {
        Customer customer = new Customer(
                newCustomer.getLastName(),
                newCustomer.getFirstName(),
                newCustomer.getPhoneNumber(),
                newCustomer.getAddress(),
                newCustomer.getCity(),
                newCustomer.getState(),
                newCustomer.getCountry(),
                newCustomer.getPostalCode(),
                newCustomer.getSalesRepEmplyeeNumber(),
                newCustomer.getCreditLimit(),
                newCustomer.getOrders(),
                newCustomer.getPayments());
        pCustomerRepository.save(customer);
        return customer;
    }

    public Object updateCustomer(Long id, Customer pCustomer) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if (customerData.isPresent()) {
            Customer _customer = customerData.get();
            _customer.setLastName(pCustomer.getLastName());
            _customer.setFirstName(pCustomer.getFirstName());
            _customer.setPhoneNumber(pCustomer.getPhoneNumber());
            _customer.setAddress(pCustomer.getAddress());
            _customer.setCity(pCustomer.getCity());
            _customer.setState(pCustomer.getState());
            _customer.setCountry(pCustomer.getCountry());
            _customer.setPostalCode(pCustomer.getPostalCode());
            _customer.setSalesRepEmplyeeNumber(pCustomer.getSalesRepEmplyeeNumber());
            _customer.setCreditLimit(pCustomer.getCreditLimit());
            _customer.setOrders(pCustomer.getOrders());
            _customer.setPayments(pCustomer.getPayments());
            pCustomerRepository.save(_customer);
            return _customer;
        }else
            return null;
    }

    public Object deleteCustomerById(Long id) {
        Optional<Customer> customerData = pCustomerRepository.findById(id);
        if(customerData.isPresent()) {
            pCustomerRepository.deleteById(id);
            return "Deleted Customer with Id Successfully";
        }else
            return null;
    }

    
}
