package com.devcamp.pizza365.controller;
import java.util.*;

import javax.validation.Valid;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.service.OrderService;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*",maxAge = -1)
public class OrderController {
    @Autowired
    OrderService orderService;
    
    @GetMapping("/order/all")
    public ResponseEntity<List<Order>> getAllOrders() {
        try {
            return new ResponseEntity<>(orderService.findAllOrders(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order/{id}")
    public ResponseEntity<Object> getOrderById(@PathVariable long id) {
        try {
            return orderService.findOrderById(id) == null
                    ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find order with id: " + id)
                    : new ResponseEntity<>(orderService.findOrderById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{customerid}/order")
    public ResponseEntity<List<Order>> getorderByCustomerId(@PathVariable long customerid) {
        try {
            return orderService.findOrderByCustomerId(customerid) == null
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(orderService.findOrderByCustomerId(customerid), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customer/{customerid}/order/create")
    public ResponseEntity<Object> createorderByCustomerId(@PathVariable long customerid, @Valid @RequestBody Order neworder) {
       try {
           return orderService.createOrder(customerid, neworder) == null
           ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Customer with id: " + customerid)
           : new ResponseEntity<>(orderService.createOrder(customerid, neworder), HttpStatus.CREATED);
       } catch (Exception e) {
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
    @DeleteMapping("/order/delete/{id}")
    public ResponseEntity<Object> deleteOrder(@PathVariable long id) {
        try {
            return orderService.deleteOrderById(id) == null
        ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Order with id: " + id)
        : ResponseEntity.status(HttpStatus.OK).body("Deleted Order Successfully");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PutMapping("/order/update/{id}")
    public ResponseEntity<Object> updateOrder(@PathVariable long id, @Valid @RequestBody  Order pOrder) {
         try {
             return orderService.updateOrder(id, pOrder) == null
            ?  ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Order with id: " + id)
            : new ResponseEntity<>(orderService.updateOrder(id, pOrder), HttpStatus.OK);
         } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
    } 
    
}
