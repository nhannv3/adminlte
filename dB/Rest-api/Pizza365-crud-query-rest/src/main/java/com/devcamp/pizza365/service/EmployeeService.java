package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Employee;
import com.devcamp.pizza365.repository.IEmployeeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {
    @Autowired
    IEmployeeRepository employeeRepository;
    
    public  List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee findEmployeeById(long id) {
        return employeeRepository.findById(id).isPresent()
        ? employeeRepository.findById(id).get()
        : null;
    }

    public Object createEmployee(Employee newEmployee) {
        employeeRepository.save(newEmployee);
        return newEmployee;
    }
    public Object updateEmployee(Long id, Employee pEmployee) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if (employeeData.isPresent()) {
            Employee _employee = employeeData.get();
          _employee.setReportTo(pEmployee.getReportTo());
          _employee.setExtension(pEmployee.getExtension());
          _employee.setFirstName(pEmployee.getFirstName());
          _employee.setJobTitle(pEmployee.getJobTitle());
          _employee.setLastName(pEmployee.getLastName());
          _employee.setOfficeCode(pEmployee.getOfficeCode());
          _employee.setEmail(pEmployee.getEmail());
      
            employeeRepository.save(_employee);
            return _employee;
        }else
            return null;
    }

    public Object deleteEmployeeById(Long id) {
        Optional<Employee> employeeData = employeeRepository.findById(id);
        if(employeeData.isPresent()) {
            employeeRepository.deleteById(id);
            return "Deleted Employee with Id Successfully";
        }else
            return null;
    }
}
