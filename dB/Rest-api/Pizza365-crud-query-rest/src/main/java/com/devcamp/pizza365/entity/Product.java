package com.devcamp.pizza365.entity;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "products")
public class Product {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@NotEmpty(message = "Input ProductCode")
    @Column(name = "product_code", nullable = false)
    private String productCode;

	@NotEmpty(message = "Input ProductName")
    @Column(name = "product_name", nullable = false)
    private String productName;

    @Column(name = "product_description")
    private String productDescription;

	@NotEmpty(message = "Input ProductScale")
    @Column(name = "product_scale", nullable = false)
    private String productScale;

	@NotEmpty(message = "Input ProductVender")
    @Column(name = "product_vendor", nullable = false)
    private String prodcutVendor;

	@NotNull(message = "Input quatityInStock")
    @Column(name = "quantity_in_stock", nullable = false)
    private Long quantityInStock;

	@NotNull(message = "Input BuyPrice")
    @Column(name = "buy_price", nullable = false)
    private BigDecimal buyprice;

    @OneToMany(mappedBy = "product", cascade = CascadeType.ALL)
    private List<OrderDetail> orderDetails;

    @ManyToOne
    @JoinColumn(name = "product_line_id")
    @JsonIgnore
    private ProductLine productLine;

    public Product() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductScale() {
        return productScale;
    }

    public void setProductScale(String productScale) {
        this.productScale = productScale;
    }

    public String getProdcutVendor() {
        return prodcutVendor;
    }

    public void setProdcutVendor(String prodcutVendor) {
        this.prodcutVendor = prodcutVendor;
    }

    public Long getQuantityInStock() {
        return quantityInStock;
    }

    public void setQuantityInStock(Long quantityInStock) {
        this.quantityInStock = quantityInStock;
    }

    public BigDecimal getBuyprice() {
        return buyprice;
    }

    public void setBuyprice(BigDecimal buyprice) {
        this.buyprice = buyprice;
    }

    public List<OrderDetail> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetail> orderDetails) {
        this.orderDetails = orderDetails;
    }
    @JsonIgnore
    public ProductLine getProductLine() {
        return productLine;
    }

    public void setProductLine(ProductLine productLine) {
        this.productLine = productLine;
    } 
}
