package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductLineService {
    @Autowired
    IProductLineRepository productLineRepository;

    public List<ProductLine> findAllProductLines() {
        return productLineRepository.findAll();
    }

    public Object findProductLineById(Long id) {
        return productLineRepository.findById(id).isPresent()
                ? productLineRepository.findById(id).get()
                : null;
    }

    public Object createProductLine(ProductLine newProductLine) {
        productLineRepository.save(newProductLine);
        return newProductLine;
    }

    public Object updateProductLine(Long id, ProductLine pProductLine) {
        Optional<ProductLine> ProductLineData = productLineRepository.findById(id);
        if (ProductLineData.isPresent()) {
            ProductLine _productLine = ProductLineData.get();
            _productLine.setDescription(pProductLine.getDescription());
            _productLine.setProducts(pProductLine.getProducts());
            productLineRepository.save(_productLine);
            return _productLine;
        }else
            return null;
    }

    public Object deleteProductLineById(Long id) {
        Optional<ProductLine> ProductLineData = productLineRepository.findById(id);
        if(ProductLineData.isPresent()) {
            productLineRepository.deleteById(id);
            return "Deleted ProductLine with Id Successfully";
        }else
            return null;
    }
}
