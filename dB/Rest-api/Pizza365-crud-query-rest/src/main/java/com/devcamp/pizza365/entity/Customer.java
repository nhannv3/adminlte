package com.devcamp.pizza365.entity;

import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "customers")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@NotEmpty(message = "Input LastName")
    @Column(name = "last_name", nullable = false)
    private String lastName;

	@NotEmpty(message = "Input FirstName")
    @Column(name = "first_name", nullable = false)
    private String firstName;

	@NotEmpty(message = "Input PhoneNumber")
    @Column(name = "phone_number", nullable = false)
    private String phoneNumber;

    @Column(name = "address")
    private String address;

    @Column(name = "city")
    private String city;

    @Column(name = "state")
    private String state;

    @Column(name = "country")
    private String country;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "sales_rep_employee_number")
    private long salesRepEmplyeeNumber;

    @Column(name = "credit_limit")
    private Long creditLimit;

    @OneToMany(mappedBy = "customer1", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Order> orders;

   

    @OneToMany(mappedBy = "customer2", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Payment> payments;

    public Customer() {
    }

    

    public Customer(String lastName, String firstName, String phoneNumber, String address, String city, String state,
            String country, String postalCode, long salesRepEmplyeeNumber, Long creditLimit, List<Order> orders,
            List<Payment> payments) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.phoneNumber = phoneNumber;
        this.address = address;
        this.city = city;
        this.state = state;
        this.country = country;
        this.postalCode = postalCode;
        this.salesRepEmplyeeNumber = salesRepEmplyeeNumber;
        this.creditLimit = creditLimit;
        this.orders = orders;
        this.payments = payments;
    }



    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public long getSalesRepEmplyeeNumber() {
        return salesRepEmplyeeNumber;
    }

    public void setSalesRepEmplyeeNumber(long salesRepEmplyeeNumber) {
        this.salesRepEmplyeeNumber = salesRepEmplyeeNumber;
    }

    public Long getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Long creditLimit) {
        this.creditLimit = creditLimit;
    }

   

    public List<Payment> getPayments() {
        return payments;
    }

    public void setPayments(List<Payment> payments) {
        this.payments = payments;
    }
    
    
}
