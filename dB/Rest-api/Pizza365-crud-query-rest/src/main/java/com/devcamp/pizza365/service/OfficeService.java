package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Office;
import com.devcamp.pizza365.repository.IOfficeRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OfficeService {
    @Autowired
    IOfficeRepository officeRepository;

    public  List<Office> findAllOffices() {
        return officeRepository.findAll();
    }

    public Office findOfficeById(long id) {
        return officeRepository.findById(id).isPresent()
        ? officeRepository.findById(id).get()
        : null;
    }

    public Object createOffice(Office newOffice) {
        officeRepository.save(newOffice);
        return newOffice;
    }
    public Object updateOffice(Long id, Office pOffice) {
        Optional<Office> OfficeData = officeRepository.findById(id);
        if (OfficeData.isPresent()) {
            Office _office = OfficeData.get();
            _office.setAdressLine(pOffice.getAdressLine());
            _office.setCity(pOffice.getCity());
            _office.setCountry(pOffice.getCountry());
            _office.setPhone(pOffice.getPhone());
            _office.setState(pOffice.getState());
            _office.setTerritory(pOffice.getTerritory());
      
            officeRepository.save(_office);
            return _office;
        }else
            return null;
    }

    public Object deleteOfficeById(Long id) {
        Optional<Office> OfficeData = officeRepository.findById(id);
        if(OfficeData.isPresent()) {
            officeRepository.deleteById(id);
            return "Deleted Office with Id Successfully";
        }else
            return null;
    }
}
