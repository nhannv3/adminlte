package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.service.ProductLineService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductLineController {
	@Autowired
    ProductLineService productLineService;

    @GetMapping("/productline/all")
    public ResponseEntity<List<ProductLine>> getAllProductLines() {
        try {
            return new ResponseEntity<>(productLineService.findAllProductLines(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/productline/{id}")
    public ResponseEntity<Object> getProductLineById(@PathVariable long id) {
        try {
            return productLineService.findProductLineById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find ProductLine with Id: " + id)
            : new ResponseEntity<>(productLineService.findProductLineById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/productline/create")
    public ResponseEntity<Object> createProductLine(@Valid @RequestBody ProductLine newProductLine) {
        try {
            return new ResponseEntity<>(productLineService.createProductLine(newProductLine), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PutMapping("/productline/update/{id}")
    public ResponseEntity<Object> updateProductLineById(@PathVariable long id, @Valid @RequestBody ProductLine pProductLine) {
        try {
            return productLineService.findProductLineById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find ProductLine with Id: " + id)
            : new ResponseEntity<>(productLineService.updateProductLine(id, pProductLine), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/productline/delete/{id}")
    public ResponseEntity<Object> deleleProductLineById(@PathVariable long id) {
       try {
        return productLineService.deleteProductLineById(id) == null
        ?  ResponseEntity.unprocessableEntity().body("Cannot find ProductLine with Id: " + id)
        : new ResponseEntity<>(HttpStatus.NO_CONTENT);
       } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    } 
}
