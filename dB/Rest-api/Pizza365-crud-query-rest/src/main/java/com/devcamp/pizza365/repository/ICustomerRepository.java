package com.devcamp.pizza365.repository;
import java.util.List;

import javax.transaction.Transactional;

import com.devcamp.pizza365.entity.Customer;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;



public interface ICustomerRepository extends JpaRepository<Customer, Long>{
    @Query(value = "SELECT * FROM customers WHERE last_name LIKE :name OR first_name LIKE :name", nativeQuery = true)
    List<Customer> findCustomerByLastNameAndFirstNameLike(@Param("name") String name);

    @Query(value = "FROM #{#entityName} WHERE city like ?1 AND state like ?2")
    List<Customer> findCustomerByCityAndState(String city,String state,Pageable pageable);


    @Query(value = "SELECT * FROM customers WHERE country like :name ORDER BY first_name ASC", nativeQuery = true)
    List<Customer> findCustomerByCountryAsc(@Param("name") String country, Pageable pageable);

    @Transactional
    @Modifying
    @Query(value = "UPDATE customers SET state = :new_state WHERE state = :pre_state ", nativeQuery = true)
    int updateCountry(@Param("new_state") String newstate,@Param("pre_state") String pre_state);

    @Query(value = "SELECT c.country, COUNT(c.last_name) countCustomer FROM customers c GROUP BY c.country", nativeQuery = true)
	List<Object> findCountCustomerByCountry(Pageable pageable);

    @Query(value = "SELECT * FROM customers", nativeQuery = true)
    List<Customer> getCustomersInPage(Pageable pageable);
}