package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.service.EmployeeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class EmployeeController {
	@Autowired
	EmployeeService employeeService;

	@GetMapping("/employee/all")
    public ResponseEntity<List<Employee>> getAllEmployees() {
        try {
            return new ResponseEntity<>(employeeService.findAllEmployees(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/employee/{id}")
    public ResponseEntity<Object> getEmployeeById(@PathVariable long id) {
        try {
            return employeeService.findEmployeeById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Employee with Id: " + id)
            : new ResponseEntity<>(employeeService.findEmployeeById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/employee/create")
    public ResponseEntity<Object> createEmployee(@Valid @RequestBody Employee newEmployee) {
        try {
            return new ResponseEntity<>(employeeService.createEmployee(newEmployee), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PutMapping("/employee/update/{id}")
    public ResponseEntity<Object> updateEmployeeById(@PathVariable long id, @Valid @RequestBody Employee pEmployee) {
        try {
            return employeeService.findEmployeeById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Employee with Id: " + id)
            : new ResponseEntity<>(employeeService.updateEmployee(id, pEmployee), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/employee/delete/{id}")
    public ResponseEntity<Object> deleleEmployeeById(@PathVariable long id) {
       try {
        return employeeService.deleteEmployeeById(id) == null
        ?  ResponseEntity.unprocessableEntity().body("Cannot find Employee with Id: " + id)
        : new ResponseEntity<>(HttpStatus.NO_CONTENT);
       } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    } 
    
}
