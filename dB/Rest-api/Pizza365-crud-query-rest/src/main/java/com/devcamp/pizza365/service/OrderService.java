package com.devcamp.pizza365.service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IOrderRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {
    @Autowired
    IOrderRepository orderRepository;

    @Autowired
    ICustomerRepository customerRepository;

     public List<Order> findAllOrders() {
        return orderRepository.findAll();
    }

    public Object findOrderById(Long id) {
        return orderRepository.findById(id).isPresent()
                ? orderRepository.findById(id).get()
                : null;
    }

    public List<Order> findOrderByCustomerId(Long id) {
        return customerRepository.findById(id).isPresent()
        ? customerRepository.findById(id).get().getOrders()
        : null;
    }

    public Object createOrder(Long customerid,Order newOrder) {
        Optional<Customer> customerData = customerRepository.findById(customerid) ;
        if(customerData.isPresent()) {
            newOrder.setCustomer(customerData.get());
            newOrder.setOrdeDate(new Date());
            newOrder.setRequiredDate(newOrder.getRequiredDate());
            newOrder.setShippedDate(newOrder.getShippedDate());
            orderRepository.save(newOrder);
            return newOrder;
        }else
            return null;
    }

    public Object updateOrder(Long id, Order pOrder) {
        Optional<Order> orderData = orderRepository.findById(id);
        if(orderData.isPresent()) {
            Order _order = orderData.get();
            _order.setComments(pOrder.getComments());
            // _order.setCustomer(pOrder.getCustomer());
            // _order.setOrdeDate(pOrder.getOrdeDate());
            _order.setOrderDetails(pOrder.getOrderDetails());
            _order.setRequiredDate(pOrder.getRequiredDate());
            _order.setShippedDate(pOrder.getShippedDate());
            _order.setStatus(pOrder.getStatus());
            orderRepository.save(_order);
            return _order;
        }else
            return null;
    }

    public Object deleteOrderById(Long id) {
        Optional<Order> OrderData = orderRepository.findById(id);
        if(OrderData.isPresent()) {
            orderRepository.deleteById(id);
            return "Deleted Order with Id Successfully";
        }else
            return null;
    }
}
