package com.devcamp.pizza365.repository;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.model.IPayCustomer;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface IPaymentReppository extends JpaRepository<Payment, Long>{
    @Query(value = "SELECT CONCAT(c.first_name,' ', c.last_name) fullname, p.* FROM payments p INNER JOIN customers c ON p.customer_id = c.id", nativeQuery = true)
    List<IPayCustomer> findCustomerFullName(Pageable pageable);
}
