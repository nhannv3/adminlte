package com.devcamp.pizza365.entity;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
@Entity
@Table(name = "offices")
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

	@NotEmpty(message = "Input City")
    @Column(name = "city", nullable = false)
    private String city;

	@NotEmpty(message = "Input Phone")
    @Column(name = "phone", nullable = false)
    private String phone;

	@NotEmpty(message = "Input AdressLine")
    @Column(name = "address_line", nullable = false)
    private String adressLine;

    @Column(name = "state")
    private String state;

	@NotEmpty(message = "Input Country")
    @Column(name = "country", nullable = false)
    private String country;

    @Column(name = "territory")
    private String territory;

    public Office() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAdressLine() {
        return adressLine;
    }

    public void setAdressLine(String adressLine) {
        this.adressLine = adressLine;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    
}
