package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Order;
import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.repository.IOrderDetailRepository;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailService {
    @Autowired
    IOrderRepository orderRepository;

    @Autowired
    IOrderDetailRepository OrderDetailRepository;

    @Autowired
    IProductRepository productRepository;

     public List<OrderDetail> findAllOrderDetails() {
        return OrderDetailRepository.findAll();
    }

    public Object findOrderDetailById(Long id) {
        return OrderDetailRepository.findById(id).isPresent()
                ? OrderDetailRepository.findById(id).get()
                : null;
    }

    public List<OrderDetail> findOrderDetailByOrderId(Long id) {
        return orderRepository.findById(id).isPresent()
        ? orderRepository.findById(id).get().getOrderDetails()
        : null;
    }

    public Object createOrderDetail(Long orderid, Long productid ,OrderDetail newOrderDetail) {
        Optional<Order> orderData = orderRepository.findById(orderid) ;
        Optional<Product> productData = productRepository.findById(productid) ;
        if(orderData.isPresent() && productData.isPresent()) {
            newOrderDetail.setOrder(orderData.get());
            newOrderDetail.setProduct(productData.get());
            OrderDetailRepository.save(newOrderDetail);
            return newOrderDetail;
        }else
            return null;
    }

    public Object updateOrderDetail(Long id, OrderDetail pOrderDetail) {
        Optional<OrderDetail> OrderDetailData = OrderDetailRepository.findById(id);
        if(OrderDetailData.isPresent()) {
            OrderDetail _orderDetail = OrderDetailData.get();
            _orderDetail.setOrder(pOrderDetail.getOrder());
            _orderDetail.setPriceEach(pOrderDetail.getPriceEach());
            _orderDetail.setProduct(pOrderDetail.getProduct());
            _orderDetail.setQuantityOrder(pOrderDetail.getQuantityOrder());
            OrderDetailRepository.save(_orderDetail);
            return _orderDetail;
        }else
            return null;
    }

    public Object deleteOrderDetailById(Long id) {
        Optional<OrderDetail> OrderDetailData = OrderDetailRepository.findById(id);
        if(OrderDetailData.isPresent()) {
            OrderDetailRepository.deleteById(id);
            return "Deleted OrderDetail with Id Successfully";
        }else
            return null;
    }
}
