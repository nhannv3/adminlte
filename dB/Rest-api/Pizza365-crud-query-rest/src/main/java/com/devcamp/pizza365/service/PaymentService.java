package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.repository.IPaymentReppository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PaymentService {
    @Autowired
    IPaymentReppository paymentReppository;

    @Autowired
    ICustomerRepository customerRepository;

     public List<Payment> findAllPayments() {
        return paymentReppository.findAll();
    }

    public Object findPaymentById(Long id) {
        return paymentReppository.findById(id).isPresent()
                ? paymentReppository.findById(id).get()
                : null;
    }

    public List<Payment> findPaymentByCustomerId(Long id) {
        return customerRepository.findById(id).isPresent()
        ? customerRepository.findById(id).get().getPayments()
        : null;
    }

    public Object createPayment(Long customerid,Payment newPayment) {
        Optional<Customer> customerData = customerRepository.findById(customerid) ;
        if(customerData.isPresent()) {
            Payment _payment = new Payment();
            _payment.setAmmount(newPayment.getAmmount());
            _payment.setCheckNumber(newPayment.getCheckNumber());
            _payment.setNgayCapNhat(newPayment.getNgayCapNhat());
            _payment.setCustomer(customerData.get());
            paymentReppository.save(_payment);
            return _payment;
        }else
            return null;
    }

    
    public Object updatePayment(Long id, Payment pPayment) {
        Optional<Payment> paymentData = paymentReppository.findById(id);
        if(paymentData.isPresent()) {
            Payment _payment = paymentData.get();
            _payment.setAmmount(pPayment.getAmmount());
            _payment.setCheckNumber(pPayment.getCheckNumber());
            _payment.setCustomer(pPayment.getCustomer());
            _payment.setNgayCapNhat(pPayment.getNgayCapNhat());

            paymentReppository.save(_payment);
            return _payment;
        }else
            return null;
    }

    public Object deletePaymentById(Long id) {
        Optional<Payment> PaymentData = paymentReppository.findById(id);
        if(PaymentData.isPresent()) {
            paymentReppository.deleteById(id);
            return "Deleted Payment with Id Successfully";
        }else
            return null;
    }
}
