package com.devcamp.pizza365.controller;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.devcamp.pizza365.entity.Customer;
import com.devcamp.pizza365.repository.ICustomerRepository;
import com.devcamp.pizza365.service.CustomerService;
import com.devcamp.pizza365.service.ExcelExporter;
import com.microsoft.schemas.office.visio.x2012.main.PageContentsType;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*",maxAge = -1)
public class CustomerController {
    @Autowired
    CustomerService pCustomerService;

    @Autowired
    ICustomerRepository customerRep;
    @GetMapping("/customer/all")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        try {
            return new ResponseEntity<>(pCustomerService.findAllCustomers(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/customer/{id}")
    public ResponseEntity<Object> getCustomerById(@PathVariable long id) {
        try {
            return pCustomerService.findCustomerById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Customer with Id: " + id)
            : new ResponseEntity<>(pCustomerService.findCustomerById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customer/create")
    public ResponseEntity<Object> createCustomer(@Valid @RequestBody Customer newCustomer) {
        try {
            return new ResponseEntity<>(pCustomerService.createCustomer(newCustomer), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PutMapping("/customer/update/{id}")
    public ResponseEntity<Object> updateCustomerById(@PathVariable long id, @Valid @RequestBody Customer pCustomer) {
        try {
            return pCustomerService.findCustomerById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Customer with Id: " + id)
            : new ResponseEntity<>(pCustomerService.updateCustomer(id, pCustomer), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/customer/delete/{id}")
    public ResponseEntity<Object> deleleCustomerById(@PathVariable long id) {
       try {
        return pCustomerService.deleteCustomerById(id) == null
        ?  ResponseEntity.unprocessableEntity().body("Cannot find Customer with Id: " + id)
        : new ResponseEntity<>(HttpStatus.NO_CONTENT);
       } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    } 
    @GetMapping("/export/customers/excel")
	public void exportToExcel(HttpServletResponse response) throws IOException {
		response.setContentType("application/octet-stream");
		DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
		String currentDateTime = dateFormatter.format(new Date());
		String headerKey = "Content-Disposition";
		String headerValue = "attachment; filename=users_" + currentDateTime + ".xlsx";
		response.setHeader(headerKey, headerValue);
		List<Customer> customer = new ArrayList<Customer>();
		customerRep.findAll().forEach(customer::add);
		ExcelExporter excelExporter = new ExcelExporter(customer);
		excelExporter.export(response);
	}

    @GetMapping("/customer/count/country")
    public List<Object> findCountCustomerByCountry() {
        return customerRep.findCountCustomerByCountry(PageRequest.of(0, 10));
    }
   
    @GetMapping("/customer/page")
    public List<Customer> getCustomerInPage()  {
        return customerRep.getCustomersInPage(PageRequest.of(0, 10));
    }
}
