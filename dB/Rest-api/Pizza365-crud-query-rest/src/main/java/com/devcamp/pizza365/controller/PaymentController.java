package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import com.devcamp.pizza365.entity.Payment;
import com.devcamp.pizza365.model.IPayCustomer;
import com.devcamp.pizza365.repository.IPaymentReppository;
import com.devcamp.pizza365.service.PaymentService;

import org.springframework.beans.factory.annotation.*;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class PaymentController {
    @Autowired
    PaymentService paymentService;

    @Autowired
    IPaymentReppository paymentReppository;

    @GetMapping("/payment/all")
    public ResponseEntity<List<Payment>> getAllPayments() {
        try {
            return new ResponseEntity<>(paymentService.findAllPayments(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/payment/{id}")
    public ResponseEntity<Object> getPaymentById(@PathVariable long id) {
        try {
            return paymentService.findPaymentById(id) == null
                    ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Payment with id: " + id)
                    : new ResponseEntity<>(paymentService.findPaymentById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customer/{customerid}/payment")
    public ResponseEntity<List<Payment>> getPaymentByCustomerId(@PathVariable long customerid) {
        try {
            return paymentService.findPaymentByCustomerId(customerid) == null
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(paymentService.findPaymentByCustomerId(customerid), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/customer/{customerid}/payment/create")
    public ResponseEntity<Object> createPaymentByCustomerId(@PathVariable long customerid, @Valid @RequestBody Payment newPayment) {
       try {
           return paymentService.createPayment(customerid, newPayment) == null
           ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Customer with id: " + customerid)
           : new ResponseEntity<>(paymentService.createPayment(customerid, newPayment), HttpStatus.CREATED);
       } catch (Exception e) {
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
    @DeleteMapping("/payment/delete/{id}")
    public ResponseEntity<Object> deletePayment(@PathVariable long id) {
        try {
            return paymentService.deletePaymentById(id) == null
        ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Payment with id: " + id)
        : ResponseEntity.status(HttpStatus.OK).body("Deleted Payment Successfully");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    @PutMapping("/payment/update/{id}")
    public ResponseEntity<Object> updatePayment(@PathVariable long id, @Valid Payment pPayment) {
         try {
             return paymentService.updatePayment(id, pPayment) == null
            ?  ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Payment with id: " + id)
            : new ResponseEntity<>(paymentService.updatePayment(id, pPayment), HttpStatus.OK);
         } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
    } 
    @GetMapping("/payment/all/customerfullname")
    public List<IPayCustomer> findCustomerFullname() {
        return paymentReppository.findCustomerFullName(PageRequest.of(0, 10));
    }
}
