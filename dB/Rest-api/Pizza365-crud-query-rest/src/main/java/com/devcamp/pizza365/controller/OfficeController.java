package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.service.OfficeService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OfficeController {
	@Autowired
	OfficeService officeService;

	@GetMapping("/office/all")
    public ResponseEntity<List<Office>> getAllOffices() {
        try {
            return new ResponseEntity<>(officeService.findAllOffices(), HttpStatus.OK);

        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @GetMapping("/office/{id}")
    public ResponseEntity<Object> getOfficeById(@PathVariable long id) {
        try {
            return officeService.findOfficeById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Office with Id: " + id)
            : new ResponseEntity<>(officeService.findOfficeById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/office/create")
    public ResponseEntity<Object> createOffice(@Valid @RequestBody Office newOffice) {
        try {
            return new ResponseEntity<>(officeService.createOffice(newOffice), HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @PutMapping("/office/update/{id}")
    public ResponseEntity<Object> updateOfficeById(@PathVariable long id, @Valid @RequestBody Office pOffice) {
        try {
            return officeService.findOfficeById(id) == null
            ?  ResponseEntity.unprocessableEntity().body("Cannot find Office with Id: " + id)
            : new ResponseEntity<>(officeService.updateOffice(id, pOffice), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 
    
    @DeleteMapping("/office/delete/{id}")
    public ResponseEntity<Object> deleleOfficeById(@PathVariable long id) {
       try {
        return officeService.deleteOfficeById(id) == null
        ?  ResponseEntity.unprocessableEntity().body("Cannot find Office with Id: " + id)
        : new ResponseEntity<>(HttpStatus.NO_CONTENT);
       } catch (Exception e) {
        return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    } 
}
