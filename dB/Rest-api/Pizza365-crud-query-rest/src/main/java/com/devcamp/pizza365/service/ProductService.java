package com.devcamp.pizza365.service;

import java.util.List;
import java.util.Optional;

import com.devcamp.pizza365.entity.Product;
import com.devcamp.pizza365.entity.ProductLine;
import com.devcamp.pizza365.repository.IProductLineRepository;
import com.devcamp.pizza365.repository.IProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductService {
    @Autowired
    IProductRepository productRepository;

    @Autowired
    IProductLineRepository productLineRepository;

     public List<Product> findAllProducts() {
        return productRepository.findAll();
    }

    public Object findProductById(Long id) {
        return productRepository.findById(id).isPresent()
                ? productRepository.findById(id).get()
                : null;
    }

    public List<Product> findProductByProductlineId(Long id) {
        return productLineRepository.findById(id).isPresent()
        ? productLineRepository.findById(id).get().getProducts()
        : null;
    }

    public Object createProduct(Long productlineid,Product newProduct) {
        Optional<ProductLine> productLineData = productLineRepository.findById(productlineid) ;
        if(productLineData.isPresent()) {
            newProduct.setProductLine(productLineData.get());
            productRepository.save(newProduct);
            return newProduct;
        }else
            return null;
    }

    public Object updateProduct(Long id, Product pProduct) {
        Optional<Product> ProductData = productRepository.findById(id);
        if(ProductData.isPresent()) {
            Product _product = ProductData.get();
            _product.setBuyprice(pProduct.getBuyprice());
            _product.setOrderDetails(pProduct.getOrderDetails());
            _product.setProdcutVendor(pProduct.getProdcutVendor());
            _product.setProductCode(pProduct.getProductCode());
            _product.setProductDescription(pProduct.getProductDescription());
            _product.setProductLine(pProduct.getProductLine());
            _product.setProductName(pProduct.getProductName());
            _product.setProductScale(pProduct.getProductScale());
            _product.setQuantityInStock(pProduct.getQuantityInStock());

            productRepository.save(_product);
            return _product;
        }else
            return null;
    }

    public Object deleteProductById(Long id) {
        Optional<Product> ProductData = productRepository.findById(id);
        if(ProductData.isPresent()) {
            productRepository.deleteById(id);
            return "Deleted Product with Id Successfully";
        }else
            return null;
    }
}
