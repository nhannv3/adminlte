package com.devcamp.pizza365.entity;

import java.math.BigDecimal;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;
@Entity
@Table(name = "order_details")
public class OrderDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull(message = "Input QuantityOrder")
    @Column(name = "quantity_order", nullable = false)
    private long quantityOrder;

    @NotNull(message = "Input Price Each")
    @Column(name = "price_each", nullable = false)
    private BigDecimal priceEach;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id", nullable = false)
    @JsonIgnore
    private Order order;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", nullable = false)
    @JsonIgnore
    private Product product;

    public OrderDetail() {
    }

    public OrderDetail(long quantityOrder, BigDecimal priceEach, Order order, Product product) {
        this.quantityOrder = quantityOrder;
        this.priceEach = priceEach;
        this.order = order;
        this.product = product;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getQuantityOrder() {
        return quantityOrder;
    }

    public void setQuantityOrder(long quantityOrder) {
        this.quantityOrder = quantityOrder;
    }

    public BigDecimal getPriceEach() {
        return priceEach;
    }

    public void setPriceEach(BigDecimal priceEach) {
        this.priceEach = priceEach;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    
    
}
