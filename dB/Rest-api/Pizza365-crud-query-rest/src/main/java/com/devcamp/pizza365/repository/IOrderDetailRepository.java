package com.devcamp.pizza365.repository;


import com.devcamp.pizza365.entity.OrderDetail;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderDetailRepository extends JpaRepository<OrderDetail, Long>{
  
}
