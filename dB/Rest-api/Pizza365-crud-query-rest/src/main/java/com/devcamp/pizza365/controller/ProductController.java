package com.devcamp.pizza365.controller;

import java.util.*;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.*;
import com.devcamp.pizza365.service.ProductService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ProductController {
	@Autowired
    ProductService productService;
    
    @GetMapping("/product/all")
    public ResponseEntity<List<Product>> getAllProducts() {
        try {
            return new ResponseEntity<>(productService.findAllProducts(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable long id) {
        try {
            return productService.findProductById(id) == null
                    ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Product with id: " + id)
                    : new ResponseEntity<>(productService.findProductById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/productline/{productlineid}/product")
    public ResponseEntity<List<Product>> getProductByProductlineId(@PathVariable long productlineid) {
        try {
            return productService.findProductByProductlineId(productlineid) == null
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(productService.findProductByProductlineId(productlineid), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/productline/{productlineid}/product/create")
    public ResponseEntity<Object> createProductByproductlineId(@PathVariable long productlineid, @Valid @RequestBody Product newProduct) {
       try {
           return productService.createProduct(productlineid, newProduct) == null
           ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Productline with id: " + productlineid)
           : new ResponseEntity<>(productService.createProduct(productlineid, newProduct), HttpStatus.CREATED);
       } catch (Exception e) {
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
    @DeleteMapping("/product/delete/{id}")
    public ResponseEntity<Object> deleteProduct(@PathVariable long id) {
        try {
            return productService.deleteProductById(id) == null
        ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Product with id: " + id)
        : ResponseEntity.status(HttpStatus.OK).body("Deleted Product Successfully");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PutMapping("/product/update/{id}")
    public ResponseEntity<Object> updateProduct(@PathVariable long id, @Valid Product pProduct) {
         try {
             return productService.updateProduct(id, pProduct) == null
            ?  ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find Product with id: " + id)
            : new ResponseEntity<>(productService.updateProduct(id, pProduct), HttpStatus.OK);
         } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
    } 
}
