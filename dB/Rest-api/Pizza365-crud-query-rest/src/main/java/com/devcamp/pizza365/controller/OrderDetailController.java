package com.devcamp.pizza365.controller;
import java.util.*;

import javax.validation.Valid;

import com.devcamp.pizza365.entity.OrderDetail;
import com.devcamp.pizza365.repository.IOrderDetailRepository;
import com.devcamp.pizza365.service.OrderDetailService;

import org.springframework.beans.factory.annotation.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
@RestController
@RequestMapping("/")
@CrossOrigin(value = "*",maxAge = -1)
public class OrderDetailController {
    @Autowired
    OrderDetailService OrderDetailService ;

    @Autowired
    IOrderDetailRepository orderDetailRepository;
    @GetMapping("/orderdetail/all")
    public ResponseEntity<List<OrderDetail>> getAllOrderDetails() {
        try {
            return new ResponseEntity<>(OrderDetailService.findAllOrderDetails(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/orderdetail/{id}")
    public ResponseEntity<Object> getOrderDetailById(@PathVariable long id) {
        try {
            return OrderDetailService.findOrderDetailById(id) == null
                    ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find OrderDetail with id: " + id)
                    : new ResponseEntity<>(OrderDetailService.findOrderDetailById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order/{orderid}/orderdetail")
    public ResponseEntity<List<OrderDetail>> getOrderDetailByOrderId(@PathVariable long orderid) {
        try {
            return OrderDetailService.findOrderDetailByOrderId(orderid) == null
                    ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
                    : new ResponseEntity<>(OrderDetailService.findOrderDetailByOrderId(orderid), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/order/{orderid}/product/{productid}/orderdetail/create")
    public ResponseEntity<Object> createOrderDetailByorderId(@PathVariable long orderid, @PathVariable long productid, @Valid @RequestBody OrderDetail newOrderDetail) {
       try {
           return OrderDetailService.createOrderDetail(orderid,productid, newOrderDetail) == null
           ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find order with id: " + orderid + "and Product with id: " + productid)
           : new ResponseEntity<>(OrderDetailService.createOrderDetail(orderid,productid, newOrderDetail), HttpStatus.CREATED);
       } catch (Exception e) {
           return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
       }
    }
    @DeleteMapping("/orderdetail/delete/{id}")
    public ResponseEntity<Object> deleteOrderDetail(@PathVariable long id) {
        try {
            return OrderDetailService.deleteOrderDetailById(id) == null
        ? ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find OrderDetail with id: " + id)
        : ResponseEntity.status(HttpStatus.OK).body("Deleted OrderDetail Successfully");
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    } 

    @PutMapping("/orderdetail/update/{id}")
    public ResponseEntity<Object> updateOrderDetail(@PathVariable long id, @Valid OrderDetail pOrderDetail) {
         try {
             return OrderDetailService.updateOrderDetail(id, pOrderDetail) == null
            ?  ResponseEntity.status(HttpStatus.NOT_FOUND).body("Cannot find OrderDetail with id: " + id)
            : new ResponseEntity<>(OrderDetailService.updateOrderDetail(id, pOrderDetail), HttpStatus.OK);
         } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
         }
    } 
    
}
