package com.devcamp.pizza365.repository;



import com.devcamp.pizza365.entity.Order;

import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrderRepository extends JpaRepository<Order, Long>{
   
}
  