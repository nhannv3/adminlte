-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 10, 2022 at 12:59 PM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devcamp_book`
--

-- --------------------------------------------------------

--
-- Table structure for table `chapters`
--

CREATE TABLE `chapters` (
  `id` bigint(20) NOT NULL,
  `chapter_info` varchar(255) DEFAULT NULL,
  `chapter_name` varchar(255) DEFAULT NULL,
  `page` varchar(255) DEFAULT NULL,
  `translator` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chapters`
--

INSERT INTO `chapters` (`id`, `chapter_info`, `chapter_name`, `page`, `translator`) VALUES
(9, 'Học và viết các số đếm đến 10. Học, vẽ và tô hình vuông, tròn', 'Bài 1: Các số đếm 10. Hình vuông, hình tròn', '3', 'Đỗ Đình Hoan'),
(10, 'thực hiện phép cộng, phép trừ trong phạm vi 10', 'Bài 2: Phép cộng, phép trừ trong phạm vi 10', '43', 'Nguyễn Ang'),
(11, 'Thực hiện học, viết các số trong phạm vi 100. Tính toán đo độ dài, giải các bài toán tương tự', 'Bài 3: Các số trong phạm vi 100.Đo độ dài, giải bài toán', '93', 'Đỗ Trung Hiếu'),
(12, 'Thực hiện phép cộng, phép trừ trong phạm vi 100. Đo, đọc và đếm thời gian', 'Bài 4: Phép cộng, phép trừ trong phạm vi 100. Đo thời gian', '153', 'Phạm Thanh Têm');

-- --------------------------------------------------------

--
-- Table structure for table `lessons`
--

CREATE TABLE `lessons` (
  `id` bigint(20) NOT NULL,
  `lesson_info` varchar(255) DEFAULT NULL,
  `lesson_name` varchar(255) DEFAULT NULL,
  `page` varchar(255) NOT NULL,
  `chapter_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `lessons`
--

INSERT INTO `lessons` (`id`, `lesson_info`, `lesson_name`, `page`, `chapter_id`) VALUES
(11, 'thực hiện so sánh các vật, học phép so sánh nhiều hơn, ít hơn', 'Nhiều hơn, ít hơn', '6', 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `chapters`
--
ALTER TABLE `chapters`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_3v44s1yv6sgyv6mhn45ie4b5` (`chapter_name`);

--
-- Indexes for table `lessons`
--
ALTER TABLE `lessons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_5tj43gyo7t4vetks0xnxtbht` (`lesson_name`),
  ADD KEY `FKmb78vk1f2oljr16oj1hpo45ma` (`chapter_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `chapters`
--
ALTER TABLE `chapters`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `lessons`
--
ALTER TABLE `lessons`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `lessons`
--
ALTER TABLE `lessons`
  ADD CONSTRAINT `FKmb78vk1f2oljr16oj1hpo45ma` FOREIGN KEY (`chapter_id`) REFERENCES `chapters` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
